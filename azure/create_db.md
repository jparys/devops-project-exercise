### In order to create a CosmosDB Azure follow the steps:

### 1 Login to azure 

```bash
 $ az login 
```

### 2 Create new CosmosDB Account 

```bash
 $ az cosmosdb create --name jp-todo-cosmos-account --resource-group UBS21_JakubParys_ProjectExercise --kind MongoDB --capabilities EnableServerless --server-version 3.6 
```

### 3 Create MongoDB database 

```bash
 $ az cosmosdb mongodb database create --account-name jp-todo-cosmos-account --name jp_todo_db --resource-group UBS21_JakubParys_ProjectExercise
```

### 4 Get connection string 

```bash
 $ az cosmosdb keys list -n jp-todo-cosmos-account -g UBS21_JakubParys_ProjectExercise --type connection-strings
``` 