### In order to create a Web App in Azure follow the steps:

### 1 Login to azure 

```bash
 $ az login 
```

az storage account create --resource-group UBS21_JakubParys_ProjectExercise --name sajpterraformtodoapp123 --sku Standard_LRS --encryption-services blob

az storage container create --name tfstate --account-name sajpterraformtodoapp123