### In order to create a Web App in Azure follow the steps:

### 1 Login to azure 

```bash
 $ az login 
```

### 2 Create an App Service Plan

```bash
 $ az appservice plan create --resource-group UBS21_JakubParys_ProjectExercise -n appsrvice_plan_jp_todo_app --sku B1 --is-linux
```

### 3 Create the Web App

```bash
 $ az webapp create 
    --resource-group UBS21_JakubParys_ProjectExercise 
    --plan appsrvice_plan_jp_todo_app --name jp-todo-app 
    --deployment-container-image-name parysj/todo-app:prod
```

### 4 Set up environment variables 


> Please provide secrets for variables
```bash
 $ az webapp config appsettings set -g UBS21_JakubParys_ProjectExercise -n jp-todo-app --settings FLASK_APP=todo_app/app
 $ az webapp config appsettings set -g UBS21_JakubParys_ProjectExercise -n jp-todo-app --settings TOKEN=
 $ az webapp config appsettings set -g UBS21_JakubParys_ProjectExercise -n jp-todo-app --settings API_KEY=
 $ az webapp config appsettings set -g UBS21_JakubParys_ProjectExercise -n jp-todo-app --settings SECRET_KEY=secret-key
```

