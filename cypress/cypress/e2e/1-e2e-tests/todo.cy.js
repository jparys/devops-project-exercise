/// <reference types="cypress" />

describe('example to-do app', () => {
  
  after(() =>{
    cy.request('DELETE', 'http://jp-todo-app-tf-e2e.azurewebsites.net/tasks')
  })

  beforeEach(() => {
    cy.visit('http://jp-todo-app-tf-e2e.azurewebsites.net')
  })

  it('can add new todo items', () => {
    const newItem = 'New Task'
   
    cy.get('#title').type(`${newItem}`)
    cy.contains('Add').click()

    cy.get('[data-cy="to-do"]')
      .contains('New Task')
      .should('exist')
  })

  it('can move item to in progress status', () => {
    cy.get('[data-cy="to-do"]')
    .contains('Start')
    .click()

    cy.get('[data-cy="to-do"]')
    .contains('New Task')
    .should('not.exist')

    cy.get('[data-cy="doing"]')
      .contains('New Task')
      .should('exist')
  })

  it('can complete item', () => {
    cy.get('[data-cy="doing"]')
    .contains('Complete')
    .click()

    cy.get('[data-cy="doing"]')
    .contains('New Task')
    .should('not.exist')

    cy.get('[data-cy="done"]')
      .contains('New Task')
      .should('exist')
  })
})
