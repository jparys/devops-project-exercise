FROM  python:3.10.4-buster as base
#--platform=linux/amd64
RUN pip install poetry
WORKDIR /app
COPY poetry.lock pyproject.toml /app/
RUN poetry config virtualenvs.create false --local && poetry install

FROM base as production
COPY ./todo_app/ /app/todo_app
EXPOSE 8080
ENV PORT=8080
CMD  poetry run gunicorn -b 0.0.0.0:$PORT -t 30 --pythonpath /app/todo_app "app:create_app()"

FROM base as development
EXPOSE 5000
ENTRYPOINT poetry run flask run --host=0.0.0.0

FROM base as test
COPY . .
ENTRYPOINT ["/bin/sh", "-c" , "coverage run -m pytest --junitxml=report.xml && coverage report && coverage xml"]

FROM base as test-watch
COPY . .
ENTRYPOINT ["poetry", "run", "ptw"]
