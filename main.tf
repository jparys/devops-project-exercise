terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 3.8"
    } 
  }
    backend "azurerm" {
        resource_group_name  = "UBS21_JakubParys_ProjectExercise"
        # storage_account_name = "sajpterraformtodoapp123"
        # container_name       = "tfstate"
        # key                  = "${var.env}.terraform.tfstate"
    }

}
provider "azurerm" {
  features {}
}
data "azurerm_resource_group" "main" {
  name     = var.resource_group
}

resource "azurerm_service_plan" "main" {
  name                = "asp-${var.env}"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "jp-todo-app-tf-${var.env}"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id
  virtual_network_subnet_id = azurerm_subnet.main.id
  site_config {

    vnet_route_all_enabled = true 
    application_stack {
      docker_image     = "parysj/todo-app"
      docker_image_tag = "prod"
    }
  }
  app_settings = {
    "DOCKER_REGISTRY_SERVER_URL" = "https://index.docker.io"
    "DOCKER_ENABLE_CI" = "true"
    "FLASK_APP" = "todo_app/app"
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
    "SECRET_KEY" = "${var.SECRET_KEY}"
    "GIT_HUB_CLIENT_ID" = "${var.GIT_HUB_CLIENT_ID}"
    "GIT_HUB_CLIENT_SECRET" = "${var.GIT_HUB_CLIENT_SECRET}" 
    "GIT_LAB_CLIENT_ID" = "${var.GIT_LAB_CLIENT_ID}"
    "GIT_LAB_CLIENT_SECRET" = "${var.GIT_LAB_CLIENT_SECRET}"
    "DB_NAME" = azurerm_cosmosdb_mongo_database.main.name
    "MONGO_CONNECTION_STRING" = azurerm_cosmosdb_account.main.connection_strings[0]
    "LOGIN_DISABLED" = var.LOGIN_DISABLED
  }  
}

resource "azurerm_cosmosdb_account" "main" {
  name                = "cosmos-db-account-tf-${var.env}"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  offer_type          = "Standard"
  kind                = "MongoDB"
  is_virtual_network_filter_enabled     = true 
  enable_automatic_failover = true

  capabilities { 
    name = "EnableServerless" 
  }

  capabilities {
    name = "EnableAggregationPipeline"
  }

  capabilities {
    name = "mongoEnableDocLevelTTL"
  }

  capabilities {
    name = "MongoDBv3.4"
  }

  capabilities {
    name = "EnableMongo"
  }

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 300
    max_staleness_prefix    = 100000
  }

  geo_location {
    location          = "uksouth" 
    failover_priority = 0
  }

  virtual_network_rule {
    id  = azurerm_subnet.main.id
    ignore_missing_vnet_service_endpoint = false 
  }
}

resource "azurerm_cosmosdb_mongo_database" "main" {
  name                = "cosmos-mongo-db-tf-${var.env}"
  resource_group_name = data.azurerm_resource_group.main.name
  account_name        = azurerm_cosmosdb_account.main.name
}

resource "azurerm_virtual_network" "main" {
  name                = "vnet-${var.env}"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
}

resource "azurerm_subnet" "main" {
  name                 = "subnet-${var.env}"
  resource_group_name  = data.azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.1.0/24"]
  service_endpoints    = [
          "Microsoft.AzureCosmosDB",
        ]
  delegation {
    name = "example-delegation"

    service_delegation {
      name    = "Microsoft.Web/serverFarms"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}