from enum import Enum

class Role(Enum):
    Admin = 'admin'
    Reader = 'reader'
    Writer = 'writer'
