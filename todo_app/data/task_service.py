from todo_app.data.item import Item
from todo_app.data.repositories import TaskRepository
import todo_app.data.task_status as status


class TaskService:
    def __init__(self, repository: TaskRepository):
        self._repository = repository

    def get_all(self):
        result = []
        for item in self._repository.get_all():
            user = Item.from_dict(item)
            result.append(user)

        return result

    def add_item(self, title: str):
        task = {
            "name": title,
            "status": status.TO_DO
        }
        self._repository.add(task)

    def complete_item(self, item_id: str):

        self._repository.update(item_id, {'status': status.DONE})

    def open_item(self, item_id: str):
        self._repository.update(item_id, {'status': status.TO_DO})

    def start_item(self, item_id: str):
        self._repository.update(item_id, {'status': status.DOING})

    def delete_all(self):
        return self._repository.delete_all()
