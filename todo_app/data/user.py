from flask_login import UserMixin


class User (UserMixin):
    def __init__(self, id, ext_user_id, user_name='', ext_system='gitlab', roles=[]):
        self.id = id
        self.roles = roles
        self.user_name = user_name
        self.ext_user_id = ext_user_id
        self.ext_system = ext_system

        self.is_admin = 'admin' in self.roles
        self.is_reader = 'reader' in self.roles
        self.is_writer = 'writer' in self.roles

    def to_dict(self):
        result = {
            "_id": self.id,
            "roles": self.roles,
            "ext_user_id": self.ext_user_id,
            "user_name": self.user_name,
            "ext_system": self.ext_system
        }
        return result

    @classmethod
    def from_dict(cls, dict):
        if(dict == None):
            return None
        user = cls(str(dict["_id"]), dict['ext_user_id'],
                   dict['user_name'], dict['ext_system'], dict['roles'])
        return user
