from todo_app.data.user import User


class UserViewModel:
    def __init__(self, items: list[User]):
        self._users = items

    @property
    def users(self) -> list[User]:
        return self._users
