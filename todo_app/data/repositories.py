from typing import Collection
from bson.objectid import ObjectId
from pymongo.collection import Collection

import pymongo
import certifi

from todo_app.config.flask_config import get_app_config
class Repository:
    def __init__(self) -> None:

        self.config = get_app_config()

        if (self.config.configuration == "LOCAL_DOCKER"):
            self._client = pymongo.MongoClient(self.config.mongo_connection_string)
        else:
            self._client = pymongo.MongoClient(
                self.config.mongo_connection_string, tlsCAFile=certifi.where())
        self.db = self._client[self.config.db_name]
        self.collection: Collection = None

        try:
            collection_name = self.get_collection_name()
            self.collection = self.db[collection_name]
        except AttributeError:
            raise NotImplementedError(
                "No `get_collection_name` method - override `Repository  __init__`") from None

    def get_by_id(self, id):
        query = {'_id': ObjectId(id)}
        return self.collection.find_one(query)

    def get_all(self):
        return self.collection.find()

    def add(self, new_item):
        return self.collection.insert_one(new_item)

    def update(self,id,to_update: dict): 
        query = {'_id': ObjectId(id)}
        setter = {"$set": to_update}
        return self.collection.update_one(query, setter)
    
    def delete_all(self):
        return self.collection.delete_many({})


class UserRepository(Repository):
    def __init__(self) -> None:
        super().__init__()

    def get_collection_name(self):
        return 'users'

    def get_by_ext_id(self, ext_id, ext_system):
        query = {'ext_user_id': ext_id, 'ext_system': ext_system}
        return self.collection.find_one(query)

class TaskRepository(Repository):   
    def __init__(self) -> None:
        super().__init__()
    
    def get_collection_name(self):
        return 'tasks'
