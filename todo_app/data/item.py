from datetime import date

class Item:
    def __init__(self, id, name, status='To Do', last_modified=date.today):
        self.id = str(id)
        self.name = name
        self.status = status
        self.last_modified = last_modified

    
    def to_dict(self):
        result = {
            "_id": self.id,
            "name": self.name,
            "status": self.status,
            "last_modified": self.last_modified
        }
        return result

    @classmethod
    def from_dict(cls, dict):
        if(dict == None):
            return None
        user = cls(str(dict["_id"]), dict['name'],
                   dict['status'])
        return user

