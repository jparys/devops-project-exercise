from todo_app.data.repositories import UserRepository
from todo_app.data.user import User


class UserService:
    def __init__(self, repository: UserRepository):
        self._repository = repository

    def get_user(self, user_id) -> User:
        user = self._repository.get_by_id(id=user_id)
        return User.from_dict(user)

    def get_all_users(self):
        result = []
        for item in self._repository.get_all():
            user = User.from_dict(item)
            result.append(user)

        return result

    def update_roles(self, user_id, roles):
        to_update = {"roles": roles}
        self._repository.update(user_id, to_update)

    def _create_user(self, ext_user_id, user_name, ext_system) -> User:
        all_user = self.get_all_users()
        roles = []
        if(len(all_user) == 0):
            roles = ['admin', 'reader', 'writer']
        else:
            roles = ['reader', 'writer']

        new_user = {
            "roles": roles,
            "ext_user_id": ext_user_id,
            "user_name": user_name,
            "ext_system": ext_system
        }
        inserted = self._repository.add(new_user)
        return self.get_user(str(inserted.inserted_id))

    def sync_external_user(self, ext_user_id, user_name, ext_system) -> User:
        user = User.from_dict(self._repository.get_by_ext_id(
            ext_id=ext_user_id, ext_system=ext_system))
        if(user == None):
            user = self._create_user(
                ext_user_id=ext_user_id, user_name=user_name, ext_system=ext_system)

        return user
