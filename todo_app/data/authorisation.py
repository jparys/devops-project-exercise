
from flask import current_app
from flask_login import current_user
from flask import render_template_string
from todo_app.data.user import User
from todo_app.data.roles import Role
from functools import wraps


def roles_required(role: str | Role):

    def decorator(function):

        @wraps(function)
        def with_roles_checking():
            if current_app.config.get("LOGIN_DISABLED") == 'True':
                func = function()
                return func
        
            cu: User = current_user
            if(isinstance(role, str)):
                if(role in cu.roles):
                    func = function()
                else:
                    func = render_template_string('Access Denied')
            else:
                if(role.value in cu.roles):
                    func = function()
                else:
                    func = render_template_string('Access Denied')

            return func

        return with_roles_checking

    return decorator
