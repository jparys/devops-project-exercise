from datetime import date
from todo_app.data.item import Item
import todo_app.data.task_status as status
from todo_app.data.user import User
from flask import current_app


class ViewModel:
    def __init__(self, items: list[Item], current_user: User =None, current_date: date = date.today(), should_show_all_done_items: bool = False):
        self._items: list[Item] = items
        self._current_date: date = current_date
        self._should_show_all_done_items = should_show_all_done_items
        self._current_user = current_user


    @property
    def todo_items(self) -> list[Item]:
        return [x for x in self._items if x.status == status.TO_DO]

    @property
    def doing_items(self) -> list[Item]:
        return [x for x in self._items if x.status == status.DOING]

    @property
    def all_done_items(self) -> list[Item]:
        return [x for x in self._items if x.status == status.DONE]

    @property
    def should_show_all_done_items(self) -> bool:
        return self._should_show_all_done_items

    @property
    def recent_done_items(self) -> list[Item]:
        return [x for x in self.all_done_items if x.last_modified == self._current_date]

    @property
    def older_done_items(self) -> list[Item]:
        return [x for x in self.all_done_items if x.last_modified < self._current_date]

    @property
    def done_items(self) -> list[Item]:
        if(len(self.all_done_items) < 5):
            return self.all_done_items

        if(self.should_show_all_done_items):
            return self.all_done_items

        return self.recent_done_items

    @property
    def can_read(self):
        if(current_app.config.get("LOGIN_DISABLED") == 'True'):
            return True

        if('reader' in self._current_user.roles):
            return True
        return False

    @property
    def can_write(self):
        if(current_app.config.get("LOGIN_DISABLED") == 'True'):
            return True

        if('writer' in self._current_user.roles):

            return True
        return False

    @property
    def user_id(self):
        return self._current_user.id
