
from flask import Flask, render_template, request, redirect, session, render_template_string
from todo_app.config.flask_config import get_app_config
from todo_app.data.authorisation import roles_required
from todo_app.data.task_service import TaskService
from todo_app.data.user import User
from todo_app.data.user_service import UserService
from todo_app.data.user_view_model import UserViewModel
from todo_app.data.view_model import ViewModel
from flask_login import LoginManager, login_required, login_user, current_user, logout_user
import requests
from todo_app.data.repositories import TaskRepository, UserRepository
from uuid import uuid4

LOGIN_GIT_HUB_CALLBACK = 'login/github/callback'
LOGIN_GIT_LAB_CALLBACK = 'login/gitlab/callback'


def create_app():
    app = Flask(__name__)
    config = get_app_config()
    app.config.from_object(config)
    task_repository = TaskRepository()
    task_service = TaskService(repository=task_repository)
    user_repository = UserRepository()
    user_service = UserService(repository=user_repository)
    login_manager = LoginManager()

    @login_manager.unauthorized_handler
    def unauthenticated():
        return redirect('/login')

    @login_manager.user_loader
    def load_user(user_id):
        return user_service.get_user(user_id)

    login_manager.init_app(app)

    @app.route('/settings')
    def settings():
        return render_template_string('{{ what }}', what=app.config)

    @app.route('/login')
    def login():
        return render_template('login.html')

    @app.route('/login/gitlab')
    def login_gitlab():
        redirect_url = f'{request.host_url}{LOGIN_GIT_LAB_CALLBACK}'
        state = str(uuid4())
        session['state'] = state
        return redirect(f'https://gitlab.com/oauth/authorize?client_id={config.git_lab_client_id}&redirect_uri={redirect_url}&response_type=code&state={state}&scope=openid+profile+email+read_user')

    @app.route('/login/github')
    def login_github():
        redirect_url = f'{request.host_url}{LOGIN_GIT_HUB_CALLBACK}'
        state = str(uuid4())
        session['state'] = state
        return redirect(f'https://github.com/login/oauth/authorize?client_id={config.git_hub_client_id}&redirect_uri={redirect_url}&response_type=code&state={state}&scope=user')

    @app.route(f'/{LOGIN_GIT_HUB_CALLBACK}')
    def login_github_callback():

        redirect_url = f'{request.host_url}{LOGIN_GIT_HUB_CALLBACK}'
        code = request.args.get('code')
        state = request.args.get('state')
        session_state = session.pop('state', None)
        if state != session_state:
            return redirect('/login')

        parameters = f'client_id={config.git_hub_client_id}&client_secret={config.git_hub_client_secret}&code={code}&redirect_uri={redirect_url}'
        url = f'https://github.com/login/oauth/access_token'
        response = requests.post(url, params=parameters, headers={
                                 'Accept': 'application/json'})
        result = response.json()

        access_token = result['access_token']

        user_response = requests.get("https://api.github.com/user", headers={
                                     'Authorization': f'Bearer {access_token}', 'Accept': 'application/json'})
        user_result = user_response.json()

        ext_user_id = user_result['id']
        user_name = user_result['login']
        ext_system = 'github'

        user: User = user_service.sync_external_user(
            ext_user_id, user_name, ext_system)

        login_user(user)

        return redirect('/')

    @app.route(f'/{LOGIN_GIT_LAB_CALLBACK}')
    def login_git_lab_callback():

        redirect_url = f'{request.host_url}{LOGIN_GIT_LAB_CALLBACK}'
        code = request.args.get('code')
        state = request.args.get('state')
        session_state = session.pop('state', None)
        if state != session_state:
            return redirect('/login')

        parameters = f'client_id={config.git_lab_client_id}&client_secret={config.git_lab_client_secret}&code={code}&grant_type=authorization_code&redirect_uri={redirect_url}'
        url = f'https://gitlab.com/oauth/token'
        response = requests.post(url, params=parameters)
        result = response.json()
        access_token = result['access_token']
        user_response = requests.get("https://gitlab.com/api/v4/user", headers={
                                     'Authorization': f'Bearer {access_token}', 'Accept': 'application/json'})
        user_json = user_response.json()
        ext_user_id = user_json['id']
        user_name = user_json['username']
        ext_system = 'gitlab'

        user: User = user_service.sync_external_user(
            ext_user_id, user_name, ext_system)

        login_user(user)

        return redirect('/')

    @app.route('/admin')
    @login_required
    @roles_required('admin')
    def admin_index():
        all_users = user_service.get_all_users()
        users_view_model = UserViewModel(all_users)
        return render_template('admin.html', view_model=users_view_model)

    
    @app.route('/admin/save', methods=['POST'])
    @login_required
    @roles_required('admin')
    def admin_save():
        user_id = request.form.get('user_id')
        is_admin = request.form.get('is_admin') in ['True', 'Changing']
        is_reader = request.form.get('is_reader') in ['True', 'Changing']
        is_writer = request.form.get('is_writer') in ['True', 'Changing']
        roles = []

        if (is_admin):
            roles.append('admin')
        if(is_writer):
            roles.append('writer')
        if(is_reader):
            roles.append('reader')

        user_service.update_roles(user_id, roles)
        return redirect('/admin')

    
    @app.route('/')
    @login_required
    @roles_required('reader')
    def index():
        all_tasks = task_service.get_all()
        view_model = ViewModel(items=all_tasks, current_user=current_user)
        return render_template('index.html', view_model=view_model)
    
    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return redirect('/login')

    
    @app.route('/', methods=['POST'])
    @login_required
    @roles_required('writer')
    def add():
        title = request.form.get('title')
        task_service.add_item(title=title)
        return redirect('/')

   
    @app.route('/complete', methods=['POST'])
    @login_required
    @roles_required('writer')
    def complete():
        item_id = request.form.get('item_id')
        task_service.complete_item(item_id)
        return redirect('/')

   
    @app.route('/start', methods=['POST'])
    @login_required
    @roles_required('writer')
    def start():
        item_id = request.form.get('item_id')
        task_service.start_item(item_id)
        return redirect('/')


    @app.route('/open', methods=['POST'])
    @login_required
    @roles_required('writer')
    def open():
        item_id = request.form.get('item_id')
        task_service.open_item(item_id)
        return redirect('/')

    @app.route('/tasks', methods=['DELETE'])
    def delete_task():
        result = task_service.delete_all()
        return render_template_string('{{ what }}', what=result)

    return app
