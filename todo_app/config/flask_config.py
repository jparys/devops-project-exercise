import os

def singleton(cls):
    instances = {}
    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return get_instance

@singleton
class Config:
    def __init__(self):
        """Base configuration variables."""
        self.SECRET_KEY = os.environ.get('SECRET_KEY')
        if not self.SECRET_KEY:
            raise ValueError("No SECRET_KEY set for Flask application. Did you follow the setup instructions?")

        self.LOGIN_DISABLED = os.environ.get('LOGIN_DISABLED')

        self.GIT_LAB_CLIENT_ID = os.environ.get('GIT_LAB_CLIENT_ID')
        if not self.GIT_LAB_CLIENT_ID:
            raise ValueError("No GIT_LAB_CLIENT_ID set for Flask application. Did you follow the setup instructions?")
        
        self.GIT_LAB_CLIENT_SECRET = os.environ.get('GIT_LAB_CLIENT_SECRET')
        if not self.GIT_LAB_CLIENT_SECRET:
            raise ValueError("No CLIENT_SECRET set for Flask application. Did you follow the setup instructions?")

        self.GIT_HUB_CLIENT_ID = os.environ.get('GIT_HUB_CLIENT_ID')
        if not self.GIT_HUB_CLIENT_ID:
            raise ValueError("No GIT_LAB_CLIENT_ID set for Flask application. Did you follow the setup instructions?")
        
        self.GIT_HUB_CLIENT_SECRET = os.environ.get('GIT_HUB_CLIENT_SECRET')
        if not self.GIT_HUB_CLIENT_SECRET:
            raise ValueError("No CLIENT_SECRET set for Flask application. Did you follow the setup instructions?")

        self.MONGO_CONNECTION_STRING = os.environ.get('MONGO_CONNECTION_STRING')
        if not self.MONGO_CONNECTION_STRING:
            raise ValueError("No MONGO_CONNECTION_STRING set. Did you follow the setup instructions?")
        
        self.DB_NAME = os.environ.get('DB_NAME')
        if not self.DB_NAME:
            raise ValueError("No DB_NAME set.")

        self.CONFIGURATION = os.environ.get('CONFIGURATION')

    
    @property    
    def mongo_connection_string(self):
        return self.MONGO_CONNECTION_STRING

    @property
    def db_name(self):
        return self.DB_NAME
    
    @property
    def configuration(self):
        return self.CONFIGURATION

    @property
    def git_lab_client_id(self):
        return self.GIT_LAB_CLIENT_ID
    
    @property
    def git_lab_client_secret(self):
        return self.GIT_LAB_CLIENT_SECRET
    
    @property
    def git_hub_client_id(self):
        return self.GIT_HUB_CLIENT_ID

    @property
    def git_hub_client_secret(self):
        return self.GIT_HUB_CLIENT_SECRET
        

def get_app_config() -> Config:
    return Config()
