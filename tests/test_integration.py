import pytest
from dotenv import load_dotenv, find_dotenv
from todo_app import app
import mongomock
import pymongo
import todo_app.data.task_status as status
from todo_app.config.flask_config import get_app_config

@pytest.fixture
def client():
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()
        with test_app.test_client() as client:
            yield client


def test_index_page(monkeypatch, client):

    config =   get_app_config()
    client_db =pymongo.MongoClient(config.mongo_connection_string)

    db = client_db[config.db_name]
    tasks = db.tasks
    task = {
            "_id" : "1",
            "name": "To_Do_Task_Test",
            "status": status.TO_DO
        }
    result = tasks.insert_one(task).inserted_id

    task = {
            "_id" : "2",
            "name": "Done_Task_Test",
            "status": status.DONE
        }
    result = tasks.insert_one(task).inserted_id

    task = {
            "_id" : "3",
            "name": "Doing_Task_Test",
            "status": status.DOING
        }
    result = tasks.insert_one(task).inserted_id

    response = client.get('/')

    assert response.status_code == 200
    assert 'To_Do_Task_Test' in response.data.decode()
    assert 'Done_Task_Test' in response.data.decode()
    assert 'Doing_Task_Test' in response.data.decode()
