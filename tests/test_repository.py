from todo_app.data.repositories import UserRepository


class StubMongoClient():

    def __init__(self):
        self._db = {
            "users" : {}
        }
        

    @property
    def db(self):
        return self._db

def test_when_pass_empty_it_should_return_empty_list() :
    # Arrange
   

    # Act
    user_repository = UserRepository()
    # Assert
    assert user_repository != None