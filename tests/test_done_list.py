from datetime import date
from todo_app.data.item import Item
from todo_app.data.user import User
from todo_app.data.view_model import ViewModel
import todo_app.data.task_status as status

current_date = date.today()


def test_when_pass_empty_it_should_return_empty_list():
    # Arrange

    view_model = ViewModel(items=[],current_user=None)

    # Act
    items = view_model.all_done_items

    # Assert
    assert len(items) == 0


def test_when_pass_other_items_it_should_return_empty_list():
    # Arrange
    items: list[Item] = []
    items.append(Item('1', 'task1', status.TO_DO))
    items.append(Item('1', 'task1', status.DOING))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items,current_user=user)


    # Act
    items = view_model.all_done_items

    # Assert
    assert len(items) == 0


def test_when_pass_done_item_it_should_return_done_list():
    # Arrange
    items: list[Item] = []
    items.append(Item('1', 'task1', status.DONE))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items,current_user=user)


    # Act
    items = view_model.all_done_items

    # Assert
    assert len(items) == 1


def test_when_pass_all_stauts_items_it_should_filter_doing_items():
    # Arrange
    items: list[Item] = []
    items.append(Item('1', 'task1', status.DOING))
    items.append(Item('1', 'task1', status.DONE))
    items.append(Item('1', 'task1', status.TO_DO))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items,current_user=user)


    # Act
    items = view_model.all_done_items

    # Assert
    assert len(items) == 1


def test_when_less_then_5_items_it_should_return_all():
    # Arrange
    items: list[Item] = []
    completed_date = date(2020, 4, 1)
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items,current_user=user,current_date=current_date, should_show_all_done_items=False)

    # Act
    items = view_model.done_items

    # Assert
    assert len(items) == 4


def test_when_more_then_5_items_it_should_return_todays_task():
    # Arrange
    items: list[Item] = []
    completed_date = date(2020, 4, 1)
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    user = User('123', ['reader'])
   
    view_model = ViewModel(items=items,current_user=user,current_date=current_date, should_show_all_done_items=False)

    # Act
    items = view_model.done_items

    # Assert
    assert len(items) == 3


def test_when_show_all_is_true_it_should_return_all():
    # Arrange
    items: list[Item] = []
    completed_date = date(2020, 4, 1)
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    user = User('123', ['reader'])
    view_model = ViewModel(
        items=items, current_user=user,current_date=current_date, should_show_all_done_items=True)

    # Act
    items = view_model.done_items

    # Assert
    assert len(items) == 6
