from todo_app.data.item import Item
from todo_app.data.user import User
from todo_app.data.view_model import ViewModel
import todo_app.data.task_status as status

def test_when_pass_empty_it_should_return_empty_list() :
    # Arrange
    view_model = ViewModel(items=[])

    # Act
    items = view_model.todo_items
    
    # Assert
    assert len(items) == 0

def test_when_pass_other_items_it_should_return_empty_list() :
    # Arrange
    items : list[Item] = []
    items.append(Item('1','task1',status.DONE))
    items.append(Item('1','task1',status.DOING))
    view_model = ViewModel(items=items)

    # Act
    items = view_model.todo_items
    
    # Assert
    assert len(items) == 0

def test_when_pass_to_do_item_it_should_return_to_do_list() :
    # Arrange
    items : list[Item] = []
    items.append(Item('1','task1',status.TO_DO))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items, current_user=user)

    # Act
    items = view_model.todo_items
    
    # Assert
    assert len(items) == 1

def test_when_pass_all_stauts_items_it_should_filter_doing_items() :
    # Arrange
    items : list[Item] = []
    items.append(Item('1','task1',status.DOING))
    items.append(Item('1','task1',status.DONE))
    items.append(Item('1','task1',status.TO_DO))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items, current_user=user)

    # Act
    items = view_model.todo_items
    
    # Assert
    assert len(items) == 1
