from datetime import date, datetime
from todo_app.data.item import Item
from todo_app.data.user import User
from todo_app.data.view_model import ViewModel
import todo_app.data.task_status as status

current_date = date.today()


def test_list():
    # Arrange
    user = User('123', ['reader'])
    view_model = ViewModel(items=[], current_user=user,
                           current_date=current_date)

    # Act
    items = view_model.recent_done_items

    # Assert
    assert len(items) == 0


def test_when_pass_todays_task_it_should_return_all_tasks():

    items: list[Item] = []

    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    items.append(Item('1', 'task1', status.DONE, current_date))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items, current_user=user,
                           current_date=current_date)

    # Act
    items = view_model.recent_done_items

    # Assert
    assert len(items) == 5


def test_when_pass_older_then_current_date_it_should_return_empty_list():

    items: list[Item] = []
    completed_date = date(2020, 4, 1)
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('1', 'task1', status.DONE, completed_date))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items, current_user=user,
                           current_date=current_date)

    # Act
    items = view_model.recent_done_items

    # Assert
    assert len(items) == 0


def test_should_filter_task_for_current_date():

    items: list[Item] = []
    completed_date = date(2020, 4, 1)
    items.append(Item('1', 'task1', status.DONE, completed_date))
    items.append(Item('2', 'task2', status.DONE, current_date))
    user = User('123', ['reader'])
    view_model = ViewModel(items=items, current_user=user,
                           current_date=current_date)

    # Act
    items = view_model.recent_done_items

    # Assert
    assert len(items) == 1
    assert items[0].id == '2'
