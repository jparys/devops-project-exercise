variable "resource_group" {
  type = string
  description = "Resource Group Name"
  default = "UBS21_JakubParys_ProjectExercise"
}

variable "env" {
  type = string
  description = "The prefix used for all resources in this environment"
  default = "prod"
}

variable "LOGIN_DISABLED" {
  default = "False"
}

variable "GIT_HUB_CLIENT_ID" {
    description = "Github Client id"
    type = string
    sensitive = true   
}

variable "GIT_HUB_CLIENT_SECRET" {
    type = string
    sensitive = true   

}

variable  "GIT_LAB_CLIENT_ID" {
    type = string
    sensitive = true   
}

variable  "GIT_LAB_CLIENT_SECRET" {
    type = string
    sensitive = true   
}

variable "SECRET_KEY" {
    type = string
    sensitive = true   
}
